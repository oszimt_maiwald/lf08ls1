package de.oszimt.ls81.csv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dariush
 *
 */
public class CSVHandler {

  /**
   * muss sich im aktuellen Ordner befinden!
   */
  private String file;
  private String delimiter;
  private String line = "";

  /**
   * Default constructor
   */
  public CSVHandler() {
    this(";", "studentNameCSV.csv");
  }

  /**
   * Standard constructor
   * 
   * @param delimiter, Trennzeichen
   * @param file,      Datei zum Einlesen
   */
  // Constructor 2
  public CSVHandler(String delimiter, String file) {
    super();
    this.delimiter = delimiter;
    this.file = file;
  }

  /**
   * Liest alle Schüler aus der csv aus und gibt sie zurück
   * 
   * @return List mit Schülern
   */
  public List<Schueler> getAll() {
    Schueler s = null;
    List<Schueler> students = new ArrayList<Schueler>();

    try {

      BufferedReader reader = new BufferedReader(new FileReader("studentNameCSV.csv"));
      String zeile = "";
      
      while ( (zeile = reader.readLine()) != null) {
//   	  System.out.println(zeile);
    	  String str = zeile;
    	  String[] wordlist = str.split(";");
    	  
    	  
    	  System.out.printf("%-10s%-10s%-10s%-10s%-10s%n", wordlist[0], wordlist[1], wordlist[2], wordlist[3], wordlist[4]);
    	  
    	  
      }

    } catch (IOException e) {

    	e.printStackTrace();

    }
    
//    public class BeispileFuerFunktionSplit {
//
//    	public static void main(String[] args) {
//    		String str= "word1 word2 word3";
//    		String[] wordlist = str.split(" ");
//    		
//    		for (String s: wordlist)
//    			System.out.println(s);
//
//    	}
//
//    }

    return students;
  }

  /**
   * Gibt alle Schüler aus
   * 
   * @param students
   */
  public void printAll(List<Schueler> students) {
    for (Schueler s : students) {
      System.out.println(s.getName());
    }
  }
}
